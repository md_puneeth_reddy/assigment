import React from 'react';
import './App.css';
import { store } from './store/store';
import { Provider } from 'react-redux';
import { FdaPanel } from './components/FdaPanel/FdaPanel';

const App: React.FC = () => {
  return (
    <div className="App">
      <Provider store={store}>
        <FdaPanel />
      </Provider>
    </div>
  );
};

export default App;
