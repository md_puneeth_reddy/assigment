import * as React from 'react';
import { useSelector } from 'react-redux';
import { Table } from 'antd';
import { InitialState } from '../../store/rootReducer';

export interface ApicallProps {}
interface StateProps {
  results: any;
}

export const MyTable: React.SFC<ApicallProps> = () => {
  const { results } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        results: state.results,
      };
    }
  );
  const columns = [
    {
      title: 'term',
      dataIndex: 'term',
      key: 'term',
    },
    {
      title: 'count',
      dataIndex: 'count',
      key: 'count',
      sorter: {
        compare: (a, b) => a.count - b.count,
      },
    },
  ];

  return (
    <div>
      <Table dataSource={results} columns={columns} />
    </div>
  );
};
