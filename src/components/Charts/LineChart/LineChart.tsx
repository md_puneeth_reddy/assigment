import { useSelector } from 'react-redux';
import React from 'react';
import { Chart, Axis, Tooltip, Geom, Legend } from 'bizcharts';
import { InitialState } from '../../../store/rootReducer';

interface StateProps {
  results: any;
}
export const LineChart = () => {
  const { results } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        results: state.results,
      };
    }
  );
  return (
    <Chart data={results} forceFit>
      <Axis name="term" />
      <Axis name="count" />
      <Legend />
      <Tooltip crosshairs={{ type: 'y' }} />
      <Geom type="line" position="term*count" size={5} />
      <Geom
        type="point"
        position="term*count"
        size={4}
        shape={'circle'}
        style={{ stroke: '#fff', lineWidth: 1 }}
      />
    </Chart>
  );
};
