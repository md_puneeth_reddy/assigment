import React, { useEffect, useState } from 'react';
import { useDispatch, Provider } from 'react-redux';
import axios from 'axios';
import { Row, Col } from 'antd';
import { RootDispatcher } from '../../store/Actions';
import { store } from '../../store/store';
import { MyTable } from '../MyTable/MyTable';
import { BarChart } from '../Charts/BarChart/BarChart';
import { LineChart } from '../Charts/LineChart/LineChart';

export const FdaPanel: React.FC = () => {
  const [noOfRecords, setNoOfRecords] = useState('10');
  const dispatch = useDispatch();
  const rootDispatcher = new RootDispatcher(dispatch);
  const fetchData = async () => {
    await axios;
    axios
      .get(
        `https://api.fda.gov/drug/event.json?count=patient.reaction.reactionmeddrapt.exact&limit=` +
          noOfRecords
      )
      .then((response) => {
        rootDispatcher.setResults(response.data.results);
      });
  };
  useEffect(() => {
    fetchData();
  }, [noOfRecords]);

  return (
    <div className="App">
      <input
        type="text"
        value={noOfRecords}
        onChange={(e) => setNoOfRecords(e.target.value)}
      ></input>
      <Provider store={store}>
        <Row>
          <Col xs={12} xl={24}>
            <div style={{ height: 300, overflow: 'auto' }}>
              <MyTable />
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs={12} xl={12}>
            <div style={{ height: 350 }}>
              <BarChart />
            </div>
          </Col>
          <Col xs={12} xl={12}>
            <div style={{ height: 350 }}>
              <LineChart />
            </div>
          </Col>
        </Row>
      </Provider>
    </div>
  );
};
