import { Dispatch } from 'redux';
import { DispatchAction, ActionType } from './rootReducer';

export class RootDispatcher {
  private readonly dispatch: Dispatch<DispatchAction>;

  constructor(dispatch: Dispatch<DispatchAction>) {
    this.dispatch = dispatch;
  }
  setResults = (payload: any) =>
    this.dispatch({ type: ActionType.SET_RESULTS, payload });
}
