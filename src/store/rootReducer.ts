import { Action, Reducer } from 'redux';

export enum ActionType {
  SET_RESULTS = 'SET_RESULTS',
}

export interface InitialState {
  results: any;
}

export const initialState: InitialState = {
  results: [],
};

export interface DispatchAction extends Action<ActionType> {
  payload: Partial<InitialState>;
}

export const rootReducer: Reducer<InitialState, DispatchAction> = (
  state = initialState,
  action
) => {
  console.log(state);
  if (action.type === ActionType.SET_RESULTS) {
    return { ...state, results: action.payload };
  } else return state;
};
